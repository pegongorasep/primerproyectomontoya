package com.montoya.primeraentregamontoya.model;

public interface Contact {
    int getId();
    String getName();
    String getLastName();
    String getEmail();
    String getPhoneNumber();
}