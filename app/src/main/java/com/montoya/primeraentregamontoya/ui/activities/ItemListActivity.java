package com.montoya.primeraentregamontoya.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;

import com.montoya.primeraentregamontoya.R;
import com.montoya.primeraentregamontoya.databinding.ActivityItemListBinding;
import com.montoya.primeraentregamontoya.db.entity.ContactEntity;
import com.montoya.primeraentregamontoya.model.Contact;
import com.montoya.primeraentregamontoya.ui.ContactAdapter;
import com.montoya.primeraentregamontoya.ui.ContactClickCallback;
import com.montoya.primeraentregamontoya.ui.fragments.ContactFragment;
import com.montoya.primeraentregamontoya.viewmodel.ContactsViewModel;

import java.util.List;

/**
 * Actividad principal que muestra la lista de contactos simple
 *
 * En caso de ser tablet muestra la lista de contactos simple y en la sección de detalle
 * muestra el fragmento de detalles de contacto
 *
 * Se uso databinding para simplificar la conexion de las vistas xml con los objectos java
 *
 * Además se uso la librería Room para manejar la base de datos SQL de manera más simple
 * y la librería LiveData para monitorear los cambios en la base de datos y para
 * guardar los valores aun en cambios de orientación del celular o en caso de que entren llamadas
 */
public class ItemListActivity extends AppCompatActivity {

    private boolean mTwoPane;
    private ContactAdapter mContactAdapter;
    ActivityItemListBinding mBinding;
    FloatingActionButton fabAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Binding para simplificar las llamadas de findViewById
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_item_list);

        Toolbar toolbar = mBinding.toolbar;
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        // Botón para agregar un nuevo contacto
        fabAdd = findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ItemListActivity.this, AddEditContactActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("contact", new ContactEntity());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        // Vemos si es tablet o telefono
        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
        }

        final ContactsViewModel viewModel = ViewModelProviders.of(this).get(ContactsViewModel.class);
        subscribeUi(viewModel.getContacts());

        View recyclerView = findViewById(R.id.item_list);
        setupRecyclerView((RecyclerView) recyclerView);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mContactAdapter = new ContactAdapter(mContactClickCallback);
        recyclerView.setAdapter(mContactAdapter);
    }

    private void subscribeUi(LiveData<List<ContactEntity>> liveData) {
        // Se uso Live data junto con Room para simplificar la base de datos y la actualización
        // de las vistas
        liveData.observe(this, myContact -> {
            if (myContact != null) {
                mBinding.setIsLoading(false);
                mContactAdapter.setContactList(myContact);
            } else {
                mBinding.setIsLoading(true);
            }
            mBinding.executePendingBindings();
        });
    }


    private final ContactClickCallback mContactClickCallback = new ContactClickCallback() {
        @Override
        public void onClick(Contact contact) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                show(contact);
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /** Mostrar el fragmento de detalles dependiendo de si es tablet o celular */
    public void show(Contact contact) {
        // Para tablets ponemos los detalles en un fragmento al lado
        if (mTwoPane) {
            ContactFragment contactFragment = ContactFragment.forContact(contact.getId());
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack("contact")
                    .replace(R.id.item_detail_container,
                            contactFragment, null).commit();

            // Para celulares ponemos los detalles en una nueva actividad
        } else {
            ContactFragment contactFragment = ContactFragment.forContact(contact.getId());
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack("contact")
                    .replace(R.id.item_master_container,
                            contactFragment, null).commit();
        }
    }
}

