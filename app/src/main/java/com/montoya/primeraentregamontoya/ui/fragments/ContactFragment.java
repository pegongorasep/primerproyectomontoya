package com.montoya.primeraentregamontoya.ui.fragments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.montoya.primeraentregamontoya.BasicApp;
import com.montoya.primeraentregamontoya.R;
import com.montoya.primeraentregamontoya.databinding.ContactFragmentBinding;
import com.montoya.primeraentregamontoya.db.DataRepository;
import com.montoya.primeraentregamontoya.db.entity.ContactEntity;
import com.montoya.primeraentregamontoya.ui.activities.AddEditContactActivity;
import com.montoya.primeraentregamontoya.viewmodel.ContactViewModel;


/**
 * Fragmento para mostrar los detalles de un contacto
 *
 * Desde aquí se pueden hacer llamadas y enviar correos con
 * intents a aplicaciones externas
 */
public class ContactFragment extends Fragment {

    private static final String KEY_CONTACT_ID = "contact_id";

    private ContactFragmentBinding mBinding;
    private FloatingActionButton fabEdit;
    private ContactEntity contactEntity;
    private FloatingActionButton fabDelete;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Databining para simplificar
        mBinding = DataBindingUtil.inflate(inflater, R.layout.contact_fragment, container, false);

        LiveData<ContactEntity> contact = null;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Integer contactId = bundle.getInt(KEY_CONTACT_ID);
            DataRepository repository = ((BasicApp) requireActivity().getApplication()).getRepository();
            contact = repository.loadContactById(contactId);
            contact.observe(getViewLifecycleOwner(), myContact -> {
                if (myContact != null) {
                    contactEntity = myContact;
                } else {
                    mBinding.setIsLoading(true);
                }
                mBinding.executePendingBindings();
            });
        }

        // Hacer llamadas
        CardView cardViewCall = mBinding.getRoot().findViewById(R.id.cv_call);
        cardViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", contactEntity.getPhoneNumber(), null));
                startActivity(intent);
            }
        });

        // Hacer mensajes
        CardView cardViewEmail = mBinding.getRoot().findViewById(R.id.cv_message);
        cardViewEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendEmail = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"+contactEntity.getEmail())); // mention an email id here
                startActivity(Intent.createChooser(sendEmail, "Elige una app"));
            }
        });

        // Editar un contacto
        fabEdit = mBinding.fabEdit;
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddEditContactActivity.class);
                Bundle bundle = new Bundle();
                if (contactEntity != null)
                    bundle.putSerializable("contact", contactEntity);
                else
                    bundle.putSerializable("contact", new ContactEntity());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        // Bprrar un contacto
        fabDelete = mBinding.fabDelete;
        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Confirmación");
                alertDialog.setMessage("Deseas Eliminar el contacto?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                DataRepository repository = ((BasicApp) getActivity().getApplication()).getRepository();
                                AsyncTask.execute(() -> {
                                    repository.deleteContact(contactEntity);

                                    NotificationManager elManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                                    NotificationCompat.Builder elBuilder = new NotificationCompat.Builder(getContext(), "CanalMontoya");
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        NotificationChannel elCanal = new NotificationChannel("CanalMontoya", "Contactos", NotificationManager.IMPORTANCE_DEFAULT);
                                        elManager.createNotificationChannel(elCanal);
                                    }
                                    elBuilder.setSmallIcon(android.R.drawable.ic_menu_save)
                                            .setContentTitle("Contacto borrado")
                                            .setContentText("El contacto se ha eliminado correctamente")
                                            .setVibrate(new long[]{0, 1000, 500, 1000})
                                            .setAutoCancel(true);
                                    elManager.notify(2, elBuilder.build());

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            getActivity().onBackPressed();
                                        }
                                    });

                                });
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ContactViewModel.Factory factory = new ContactViewModel.Factory(
                requireActivity().getApplication(), requireArguments().getInt(KEY_CONTACT_ID));

        final ContactViewModel model = new ViewModelProvider(this, factory)
                .get(ContactViewModel.class);

        mBinding.setLifecycleOwner(getViewLifecycleOwner());
        mBinding.setContactViewModel(model);
    }


    @Override
    public void onDestroyView() {
        mBinding = null;
        super.onDestroyView();
    }

    /** Creates contact fragment for specific contact ID */
    public static ContactFragment forContact(int contactId) {
        ContactFragment fragment = new ContactFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_CONTACT_ID, contactId);
        fragment.setArguments(args);
        return fragment;
    }
}
