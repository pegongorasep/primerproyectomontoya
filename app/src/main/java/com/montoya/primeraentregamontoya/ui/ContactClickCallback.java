package com.montoya.primeraentregamontoya.ui;

import com.montoya.primeraentregamontoya.model.Contact;

public interface ContactClickCallback {
    void onClick(Contact contact);
}
