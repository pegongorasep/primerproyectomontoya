package com.montoya.primeraentregamontoya.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.montoya.primeraentregamontoya.R;
import com.montoya.primeraentregamontoya.databinding.ContactItemBinding;
import com.montoya.primeraentregamontoya.databinding.ContactItemSimpleBinding;
import com.montoya.primeraentregamontoya.model.Contact;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    List<? extends Contact> mContactList;

    @Nullable
    private final ContactClickCallback mContactClickCallback;

    public ContactAdapter(@Nullable ContactClickCallback clickCallback) {
        mContactClickCallback = clickCallback;
        setHasStableIds(true);
    }

    public void setContactList(final List<? extends Contact> contactList) {
        if (mContactList == null) {
            mContactList = contactList;
            notifyItemRangeInserted(0, contactList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mContactList.size();
                }

                @Override
                public int getNewListSize() {
                    return contactList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mContactList.get(oldItemPosition).getId() ==
                            contactList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Contact newContact = contactList.get(newItemPosition);
                    Contact oldContact = mContactList.get(oldItemPosition);
                    return newContact.getId() == oldContact.getId();
                }
            });
            mContactList = contactList;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    @NonNull
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ContactItemSimpleBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.contact_item_simple,
                        parent, false);
        binding.setCallback(mContactClickCallback);
        return new ContactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        holder.binding.setContact(mContactList.get(position));
        //holder.binding.imageView.setVisibility(View.GONE);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mContactList == null ? 0 : mContactList.size();
    }

    @Override
    public long getItemId(int position) {
        return mContactList.get(position).getId();
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder {

        final ContactItemSimpleBinding binding;

        public ContactViewHolder(ContactItemSimpleBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
