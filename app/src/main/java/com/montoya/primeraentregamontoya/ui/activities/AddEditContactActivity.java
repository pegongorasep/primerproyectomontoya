package com.montoya.primeraentregamontoya.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.montoya.primeraentregamontoya.BasicApp;
import com.montoya.primeraentregamontoya.R;
import com.montoya.primeraentregamontoya.databinding.ActivityAddEditContactBinding;
import com.montoya.primeraentregamontoya.db.DataRepository;
import com.montoya.primeraentregamontoya.db.entity.ContactEntity;


/**
 * Actividad para crear o editar contactos
 * Utiliza notificaciones locales para avisar cuando se realiza una acción
 * Se le pasa el objeto de tipo ContactEntity como un serializable en los extras
 */

public class AddEditContactActivity extends AppCompatActivity {
    ActivityAddEditContactBinding mBinding;
    private FloatingActionButton fabAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_edit_contact);

        Bundle bundle = this.getIntent().getExtras();

        ContactEntity contactEntity = (ContactEntity)bundle.getSerializable("contact");

        Log.v("contact entity: ", contactEntity.toString());

        // Botón para guardar los cambios
        fabAdd = mBinding.floatingActionButton;
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(AddEditContactActivity.this).create();
                alertDialog.setTitle("Confirmación");
                alertDialog.setMessage("Desea guardar los cambios?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                contactEntity.setName(mBinding.tvName.getText().toString());
                                contactEntity.setLastName(mBinding.tvLastName.getText().toString());
                                contactEntity.setEmail(mBinding.tvEmail.getText().toString());
                                contactEntity.setPhoneNumber(mBinding.tvTelefono.getText().toString());

                                DataRepository repository = ((BasicApp) getApplication()).getRepository();
                                AsyncTask.execute(() -> {
                                    repository.insertContact(contactEntity);

                                    NotificationManager elManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                    NotificationCompat.Builder elBuilder = new NotificationCompat.Builder(AddEditContactActivity.this, "CanalMontoya");
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        NotificationChannel elCanal = new NotificationChannel("CanalMontoya", "Contactos", NotificationManager.IMPORTANCE_DEFAULT);
                                        elManager.createNotificationChannel(elCanal);
                                    }
                                    elBuilder.setSmallIcon(android.R.drawable.ic_menu_save)
                                            .setContentTitle("Contacto guardado")
                                            .setContentText("Los cambios se han guardado correctamente")
                                            .setVibrate(new long[]{0, 1000, 500, 1000})
                                            .setAutoCancel(true);
                                    elManager.notify(1, elBuilder.build());

                                    finish();
                                });
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle bundle = this.getIntent().getExtras();
        ContactEntity contactEntity = (ContactEntity)bundle.getSerializable("contact");

        mBinding.setContact(contactEntity);
    }

}
