package com.montoya.primeraentregamontoya.db;

import com.montoya.primeraentregamontoya.db.entity.ContactEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generates data to pre-populate the database
 */
public class DataGenerator {

    private static final String[] FIRST = new String[]{
            "Juan", "Carlos", "Miguel", "Pedro", "Luis"};
    private static final String[] SECOND = new String[]{
            "García", "Ramirez", "Gutierrez", "Larrañaga"};
    private static final String[] PHONE = new String[]{
            "9991234567", "1234567890",
            "4421234567", "5557654321", "5554429991"};
    private static final String[] EMAIL = new String[]{
            "hola@mail.com", "test@hotmail.com",
            "yo@gmail.com", "hi@mail.com", "hey@mail.com"};

    public static List<ContactEntity> generateContacts() {
        List<ContactEntity> contacts = new ArrayList<>(FIRST.length * SECOND.length);
        Random rnd = new Random();
        for (int i = 0; i < FIRST.length; i++) {
            for (int j = 0; j < SECOND.length; j++) {
                ContactEntity contact = new ContactEntity();
                contact.setName(FIRST[i]);
                contact.setLastName(SECOND[j]);
                contact.setEmail(EMAIL[i]);
                contact.setPhoneNumber(PHONE[i]);
                contact.setId(FIRST.length * i + j + 1);
                contacts.add(contact);
            }
        }
        return contacts;
    }
}
