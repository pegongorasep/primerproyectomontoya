package com.montoya.primeraentregamontoya.db;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.montoya.primeraentregamontoya.db.entity.ContactEntity;

import java.util.List;

/**
 * Repository handling the work with contacts.
 */
public class DataRepository {

    private static DataRepository sInstance;

    private final AppDatabase mDatabase;
    private MediatorLiveData<List<ContactEntity>> mObservableContacts;

    private DataRepository(final AppDatabase database) {
        mDatabase = database;

        mObservableContacts = new MediatorLiveData<>();
        mObservableContacts.addSource(mDatabase.contactDao().getAll(),
                new Observer<List<ContactEntity>>() {
                    @Override
                    public void onChanged(List<ContactEntity> contactEntities) {
                        if (mDatabase.getDatabaseCreated().getValue() != null) {
                            mObservableContacts.postValue(contactEntities);
                        }
                    }
                });
    }

    public static DataRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }


    /**
     * Get the list of contacts from the database and get notified when the data changes.
     */
    public LiveData<List<ContactEntity>> getContacts() {
        return mObservableContacts;
    }

    public void insertContact(ContactEntity contactEntity) {
        mDatabase.contactDao().insert(contactEntity);
    }

    public void deleteContact(ContactEntity contactEntity) {
        mDatabase.contactDao().delete(contactEntity);
    }

    public LiveData<ContactEntity> loadContact(final String name) {
        return mDatabase.contactDao().findByName(name);
    }

    public LiveData<ContactEntity> loadContactById(final int id) {
        return mDatabase.contactDao().loadContact(id);
    }

    public ContactEntity loadContact(final int id) {
        return mDatabase.contactDao().loadContactById(id);
    }
}
