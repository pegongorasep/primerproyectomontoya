package com.montoya.primeraentregamontoya.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.montoya.primeraentregamontoya.db.entity.ContactEntity;

import java.util.List;

// Nuestros métodos para usar SQL de manera más fácil
@Dao
public interface ContactDao {
    @Query("SELECT * FROM ContactEntity")
    LiveData<List<ContactEntity>> getAll();

    @Query("SELECT * FROM ContactEntity WHERE uid IN (:contactIds)")
    List<ContactEntity> loadAllByIds(int[] contactIds);

    @Query("SELECT * FROM ContactEntity WHERE first_name LIKE :first LIMIT 1")
    LiveData<ContactEntity> findByName(String first);

    @Query("select * from ContactEntity where uid = :contactId")
    LiveData<ContactEntity> loadContact(int contactId);

    @Query("select * from ContactEntity where uid = :contactId")
    ContactEntity loadContactById(int contactId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ContactEntity contactEntitie);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ContactEntity> contactsEntities);

    @Delete
    void delete(ContactEntity contactEntity);
}
