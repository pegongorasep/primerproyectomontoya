package com.montoya.primeraentregamontoya.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.montoya.primeraentregamontoya.BasicApp;
import com.montoya.primeraentregamontoya.db.DataRepository;
import com.montoya.primeraentregamontoya.db.entity.ContactEntity;

public class ContactViewModel extends AndroidViewModel {

    private final LiveData<ContactEntity> mObservableContact;

    private final int mContactId;

    public ContactViewModel(@NonNull Application application, DataRepository repository,
                            final int contactId) {
        super(application);
        mContactId = contactId;

        mObservableContact = repository.loadContactById(mContactId);
    }

    /**
     * Expose the LiveData Comments query so the UI can observe it.
     */
    public LiveData<ContactEntity> getContact() {
        return mObservableContact;
    }

    /**
     * A creator is used to inject the contact ID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the contact ID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private final int mContactId;

        private final DataRepository mRepository;

        public Factory(@NonNull Application application, int contactId) {
            mApplication = application;
            mContactId = contactId;
            mRepository = ((BasicApp) application).getRepository();
        }

        @SuppressWarnings("unchecked")
        @Override
        @NonNull
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ContactViewModel(mApplication, mRepository, mContactId);
        }
    }
}
