package com.montoya.primeraentregamontoya.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.montoya.primeraentregamontoya.BasicApp;
import com.montoya.primeraentregamontoya.db.DataRepository;
import com.montoya.primeraentregamontoya.db.entity.ContactEntity;

import java.util.List;

public class ContactsViewModel extends AndroidViewModel implements ViewModelProvider.Factory {
    private final DataRepository mRepository;
    private final LiveData<List<ContactEntity>> mContacts;

    public ContactsViewModel(@NonNull Application application) {
        super(application);

        mRepository = ((BasicApp) application).getRepository();
        mContacts = mRepository.getContacts();
    }

    public LiveData<List<ContactEntity>> getContacts() {
        return mContacts;
    }
    

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return null;
    }
}
